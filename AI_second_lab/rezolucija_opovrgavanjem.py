import os
import re
import time


start = time.process_time()
file = os.path.join(os.environ.get('HOMEPATH'), 'Desktop', 'lab2_files', 'resolution_examples', 'coffee.txt')
klauzule = dict()
klauzule_set = set()
redni_br = 1


with open(file, 'r') as f:
    for line in f:
        if re.search('#', line):
            continue
        line = re.sub(r'\n', '', line.lower())  #micanje \n
        # print(line)
        line = re.split(r'\s+v\s+', line)  #micanje disjunkcije
        klauzule.__setitem__(redni_br, line)
        redni_br += 1
i = len(klauzule)
ciljna_klauzula = klauzule.popitem()[1]
redni_br -= 1
print(klauzule, ciljna_klauzula)


def plResolution():
    global redni_br
    negative(klauzule, ciljna_klauzula)
    enabler = True
    # print(klauzule)

    while(enabler):
        new = []
        # print(selectClauses(klauzule))
        selected = selectClauses(klauzule)
        for key, value in selected.items():
            resolvents = plReslove(key, value)
            # print(resolvents)
            # resolvents.append([])
            for resolvent in resolvents:
                new.append(resolvent)
                if not resolvent:
                    print('true')
                    enabler = False
                    break
            if not enabler:
                break
        # print(new)

        brojac = 0
        for clause in new:
            new_set = set(clause)
            for key, value in klauzule.items():
                klauzula_set = set(value)
                if new_set == klauzula_set:
                    brojac += 1
                    break
        if brojac == new.__len__():
            print('false')
            enabler = False
        print(brojac, new.__len__())


        for clause in new:
            if clause not in klauzule.values():
                klauzule.__setitem__(redni_br, clause)
                redni_br = redni_br + 1

# loop do
#     for each(c1, c2) in selectClauses(clauses) do    #
#         resolvents ← plResolve(c1, c2)               #
#         if NIL ∈ resolvents then return true
#         new ← new ∪ resolvents
#     if new ⊆ clauses then return false
#     clauses ← clauses ∪ new


def plReslove(c1, c2):
    rezolvente = []
    for indeksi_klauzula in c2:
        lijeva_klauzula = klauzule.get(c1)
        desna_klauzula = klauzule.get(indeksi_klauzula)
        for literal_iz_lijeve in lijeva_klauzula:
            for literal_iz_desne in desna_klauzula:
                if re.sub(r'~', '', literal_iz_lijeve) == re.sub(r'~', '', literal_iz_desne) and literal_iz_lijeve != literal_iz_desne:
                    izraz = []
                    for lijevi in lijeva_klauzula:
                        if lijevi != literal_iz_lijeve:
                            izraz.append(lijevi)
                    for desni in desna_klauzula:
                        if desni != literal_iz_desne:
                            izraz.append(desni)
                    rezolvente.append(izraz)
                    break
    return rezolvente


def selectClauses(klauzule):
    clause_dictionary = klauzule
    selected = dict()
    for key, value in clause_dictionary.items():
        parovi = []
        for first_value in value:
            for i in range(key + 1, clause_dictionary.__len__() + 1):
                iteration = clause_dictionary.__getitem__(i)
                for second_value in iteration:
                    if re.sub(r'~', '', first_value) == re.sub(r'~', '', second_value) and first_value != second_value:
                        parovi.append(i)
        if parovi:
            selected.__setitem__(key, parovi)
    # print(selected)
    return selected


def negative(clauses, final_clause):
    global redni_br
    for item in final_clause:
        if re.search(r'~', item):
            clauses.__setitem__(redni_br, [re.sub(r'~', '', item)])
            redni_br += 1
        else:
            clauses.__setitem__(redni_br, ['~' + item])
            redni_br += 1
    return clauses


for key, value in klauzule.items():
    print(key, '. ', ' v '.join(value))
print('=============')
for value in ciljna_klauzula:
    print(i, '. ', value)
    i += 1
print('=============')



plResolution()

for key, value in klauzule.items():
    if key >= i:
        print(key, '. ', ' v '.join(value))
print('=============')


end = time.process_time()
print(end - start)



# def strategijaBrisanja(novi):
#     global klauzule
#     for key, value in klauzule.items():
#         prvi_set = set(value)
#         for item in novi:
#             drugi_set = set(item)
#             if prvi_set.issubset(drugi_set):
#                 novi.remove(item)
#     for item in novi:
#         if item.__len__() == 2 and re.sub(r'~', '', item[0]) == re.sub(r'~', '', item[1]) and item[0] != item[1]:
#             novi.remove(item)