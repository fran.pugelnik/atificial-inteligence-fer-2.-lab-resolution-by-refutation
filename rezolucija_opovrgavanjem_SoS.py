import os
import re
import time
import sys

# start = time.process_time()

version = sys.argv[1]
path = sys.argv[2]
verbose = False
resolution = False
cooking_interactive = False
cooking_test = False
if version == 'resolution':
    resolution = True
    if sys.argv.__len__() > 3:
        verbose = True
elif version == 'cooking_interactive':
    cooking_interactive = True
    if sys.argv.__len__() > 3:
        verbose = True
elif version == 'cooking_test':
    cooking_test = True


# file = os.path.join(os.environ.get('HOMEPATH'), 'Desktop', 'lab2_files', 'resolution_examples', 'coffee_or_tea.txt')
file = path
klauzule = dict()
klauzule_pocetne = dict()
SoS = dict()
redni_br = 1


with open(file, 'r') as f:
    for line in f:
        if re.search('#', line):
            continue
        line = re.sub(r'\n', '', line.lower())  #micanje \n
        # print(line)
        line = re.split(r'\s+v\s+', line)  #micanje disjunkcije
        klauzule.__setitem__(redni_br, line)
        redni_br += 1
i = len(klauzule)
ciljna_klauzula = klauzule.popitem()[1]
klauzule_pocetne = dict(klauzule)
pocetak = dict(klauzule_pocetne)
redni_br -= 1
# print(klauzule_pocetne, SoS, i)


def plResolution():
    global redni_br
    global klauzule_pocetne
    global SoS
    negative(klauzule, ciljna_klauzula)
    enabler = True
    # print(klauzule)

    while(enabler):
        new = []
        # print(selectClauses(klauzule))
        selected = selectClauses(klauzule)
        for key, value in selected.items():
            resolvents = plReslove(key, value)
            # print(resolvents)
            # resolvents.append([])
            for resolvent in resolvents:
                new.append(resolvent)
                if not resolvent:
                    return 'true'
                    enabler = False
                    break
            if not enabler:
                break
        # print(new)

        if enabler:
            strategijaBrisanja(new)

        brojac = 0
        for clause in new:
            new_set = set(clause)
            for key, value in klauzule.items():
                klauzula_set = set(value)
                if new_set == klauzula_set:
                    brojac += 1
                    break
        if brojac == new.__len__():
            return 'unknown'
            enabler = False
        # print(brojac, new.__len__())


        for clause in new:
            if clause not in klauzule.values():
                klauzule.__setitem__(redni_br, clause)
                SoS.__setitem__(redni_br, clause)
                redni_br = redni_br + 1

# loop do
#     for each(c1, c2) in selectClauses(clauses) do    #
#         resolvents ← plResolve(c1, c2)               #
#         if NIL ∈ resolvents then return true
#         new ← new ∪ resolvents
#     if new ⊆ clauses then return false
#     clauses ← clauses ∪ new


def strategijaBrisanja(novi):
    global klauzule
    global SoS
    global i
    for key, value in klauzule.items():
        prvi_set = set(value)
        for item in novi:
            drugi_set = set(item)
            if drugi_set.issubset(prvi_set):
                if key in klauzule_pocetne:
                    klauzule_pocetne.pop(key)
    for len in range(i, SoS.__len__()):
        if len in SoS:
            temp = SoS.__getitem__(len)
            prvi_set = set(temp)
            for item in novi:
                if len in SoS:
                    drugi_set = set(item)
                    if drugi_set.issubset(prvi_set):
                        SoS.pop(len)
    for item in novi:
        if item.__len__() == 2 and re.sub(r'~', '', item[0]) == re.sub(r'~', '', item[1]) and item[0] != item[1]:
            novi.remove(item)


def plReslove(c1, c2):
    rezolvente = []
    for indeksi_klauzula in c2:
        lijeva_klauzula = klauzule.get(c1)
        desna_klauzula = klauzule.get(indeksi_klauzula)
        for literal_iz_lijeve in lijeva_klauzula:
            for literal_iz_desne in desna_klauzula:
                if re.sub(r'~', '', literal_iz_lijeve) == re.sub(r'~', '', literal_iz_desne) and literal_iz_lijeve != literal_iz_desne:
                    izraz = []
                    for lijevi in lijeva_klauzula:
                        if lijevi != literal_iz_lijeve:
                            izraz.append(lijevi)
                    for desni in desna_klauzula:
                        if desni != literal_iz_desne:
                            izraz.append(desni)
                    rezolvente.append(izraz)
                    break
    return rezolvente


def selectClauses(klauzule):
    global klauzule_pocetne
    global SoS
    selected = dict()
    for key_SoS, value_SoS in SoS.items():
        parovi = []
        for key_pocetne, value_pocetne in klauzule_pocetne.items():
            for first_value in value_SoS:
                for second_value in value_pocetne:
                    if re.sub(r'~', '', first_value) == re.sub(r'~', '', second_value) and first_value != second_value:
                        # print(value_SoS, value_pocetne)
                        parovi.append(key_pocetne)
        for first_value in value_SoS:
            for item in range(key_SoS + 1, SoS.__len__() + 1):
                if item in SoS:
                    iteration = SoS.__getitem__(item)
                    for second_value in iteration:
                        if re.sub(r'~', '', first_value) == re.sub(r'~', '', second_value) and first_value != second_value:
                            parovi.append(item)
        if parovi:
            selected.__setitem__(key_SoS, parovi)
    return selected


def negative(clauses, final_clause):
    global redni_br
    global SoS
    for item in final_clause:
        if re.search(r'~', item):
            clauses.__setitem__(redni_br, [re.sub(r'~', '', item)])
            SoS.__setitem__(redni_br, [re.sub(r'~', '', item)])
            redni_br += 1
        else:
            clauses.__setitem__(redni_br, ['~' + item])
            SoS.__setitem__(redni_br, ['~' + item])
            redni_br += 1
    return clauses


if resolution:
    truth = plResolution()
    if verbose:
        for key, value in pocetak.items():
            print(key, '. ', ' v '.join(value))
        print('=============')
        for value in ciljna_klauzula:
            print(i, '. ', value)
            i += 1
        print('=============')
        for key, value in klauzule.items():
            if key >= i:
                print(key, '. ', ' v '.join(value))
        print('=============')
    print(' v '.join(ciljna_klauzula), ' is ' , truth)


if cooking_interactive:
    print()
if cooking_test:
    print()


# end = time.process_time()
# print(end - start)
